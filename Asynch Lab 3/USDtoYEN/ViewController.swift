//
//  ViewController.swift
//  USDtoYEN
//
//  Created by Chance Frederick on 1/14/18.
//  Copyright © 2018 Chance Frederick. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class ViewController: UIViewController {

    @IBAction func myButton1(_ sender: Any) {
        //Get the input from the Text Field
        let textFieldValue = Double(textField1.text!)
        
        //if statement to make sure user cannot leave this Text Field blank
        if textFieldValue != nil {
            let result = Double (textFieldValue! * rates[exchangeRateButton.currentTitle!].double!)
            
            myLabel1.text = "$\(textFieldValue!) = ¥\(result)"
            //Clear text field after clicking the button
            textField1.text = ""
        } else {
            myLabel1.text = "This field cannot be blank!"
        }
    }
    /*@IBAction func myButton1(_ sender: Any) {
        let textFieldValue = Double(textField1.text!)
        if textFieldValue != nil {
            let result = Double (textFieldValue! * rates[exchangeRateButton.currentTitle!].double!)
            myLabel1.text = "$\(textFieldValue!) = \(result)" + " \(exchangeRateButton.currentTitle!)"
            textField1.text = ""
        }
        else {
            myLabel1.text = "This field cannot be blank!"
        }*/
    }

    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var myLabel1: UILabel!
    
    var url = "https://api.fixer.io/latest?base=USD"
    var rates = JSON()
    var currencyDropDown = DropDown()
    var currencyNames = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        currencyDropDown.anchorView = exchangeRateButton
        currencyDropDown.selectionAction = { [weak self] (index, currency) in
            self?.exchangeRateButton.setTitle(currency, for: .normal)
        }
        
        func downLoadExchangeRates(_URL :String, success:@escaping (JSON)->Void, failure:@escaping (Error) -> Void){
            Alamofire.request(_URL).responseJSON{ (response) ->Void in
                if response.result.isSuccess {
                    let responseJSON = JSON(response.result.value!)
                    let ratesJSON = JSON(responseJSON["rates"])
                    success(ratesJSON)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    failure(error)
                   /* _ = response.result.error
                    print(Error.self)*/
                    
                };
            }
        }
        
        downLoadExchangeRates(_URL: url, success: { (ratesJSON) in
            self.rates = ratesJSON
            for (key,_) in self.rates{
                self.currencyNames.append(key)
            }
            self.currencyDropDown.dataSource = self.currencyNames
            self.currencyDropDown.reloadAllComponents()
            print(self.rates)
        }) { (error)  in
        print(error)
    }

        func onButtonHit(_ sender: Any) {
        currencyDropDown.show()
    }
        func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        func myButton1(_ sender: Any) {
        //Get the input from the Text Field
        let textFieldValue = Double(textField1.text!)
        
        //if statement to make sure user cannot leave this Text Field blank
        if textFieldValue != nil {
            let result = Double (textFieldValue! * rates[exchangeRateButton.currentTitle!].double!)
            
            myLabel1.text = "$\(textFieldValue!) = ¥\(result)"
            //Clear text field after clicking the button
            textField1.text = ""
        } else {
            myLabel1.text = "This field cannot be blank!"
        }
    }
    

}


